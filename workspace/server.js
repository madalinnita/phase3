


const express = require('express');
const bodyParser = require('body-parser');
const Sequelize = require('sequelize');
const users = express.Router();
const bcrypt = require('bcrypt');
const jwt = require("jsonwebtoken");


const app = express();
app.use(bodyParser.json());
app.use(bodyParser.json({type:"application/vnd.api+json"}));
app.use(bodyParser.text());
app.use(bodyParser.urlencoded({extended:true}));
app.use(express.static('./frontend/app-frontend/build'));
app.use(bodyParser.urlencoded({extended: false}))
process.env.SECRET_KEY = 'secret'

// var db = require('/cvmteam/models');
// var apiRoutes = require('/cvmteam/routes/apiRoutes.js');

const sequelize = new Sequelize('c9', 'cosmina1196', '', {
   host: 'localhost',
   dialect: 'mysql',
   operatorsAliases: false,
   pool: {
        "max": 1,
        "min": 0,
        "idle": 20000,
        "acquire": 20000
    }
});

sequelize
  .authenticate()
  .then(() => {
    console.log('Connected with success!');
  })
  .catch(err => {
    console.error('Connection failed for the database:', err);
  });


const User = sequelize.define('users', {
   name: {
       type: Sequelize.STRING,
       allowNull: false
   }, 
   surname: {
        type: Sequelize.STRING,
        allowNull: false
   },
   email: {
       type: Sequelize.STRING,
       allowNull: false,
       primaryKey: true
   },
    password : {
       type: Sequelize.STRING,
       allowNull: false
   }
});





app.use("/users", User);



const Resource = sequelize.define('resources', {
   identifier: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
   subjectName: {
      type: Sequelize.STRING,
      allowNull: false
   }, 
   category: {
       type: Sequelize.STRING,
       allowNull: false
   },
   yearOfStudy: {
       type: Sequelize.INTEGER,
       allowNull: false
   }
});

const Post = sequelize.define('posts', {
   identifier: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
   uploadedDate: {
      type: Sequelize.STRING,
      allowNull: false
   },
    postName: {
       type: Sequelize.INTEGER,
       allowNull: false
   },
   content: {
       type: Sequelize.STRING,
       allowNull: false
   },
    uploadingSource: {
       type: Sequelize.STRING,
       allowNull: false,
   },
   languageRo: {
       type: Sequelize.BOOLEAN,
       allowNull: false,
       defaultValue: false
   }
});

Post.belongsTo(Resource);

const Question = sequelize.define('questions', {
   identifier: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
   subject: {
      type: Sequelize.STRING,
      allowNull: false
   }, 
   content: {
       type: Sequelize.STRING,
       allowNull: false
   }
});

const Answer = sequelize.define('answers', {
   identifier: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
   subject: {
      type: Sequelize.STRING,
      allowNull: false
   }, 
   content: {
       type: Sequelize.STRING,
       allowNull: false
   }
});

Answer.belongsTo(Question);
Question.belongsTo(User);

sequelize.sync().then(()=>{
    console.log('Databases were created with success!');
});

app.post('/register', (req, res) =>{
    User.create({
        name: req.body.name,
        surname: req.body.surname,
        email: req.body.email,
        password: req.body.password
    }).then((user) => {
        res.status(200).send("User was created successfully");
    }, (err) => {
        res.status(500).send(err);
    })
})


// app.post('/login', (req, res) => {
//   User.findOne({where:{email: req.body.email, password: req.body.password} }).then((result) => {
//       res.status(200).send(result)
//   }) 
// });

app.post('/add-resource', (req,res) => {
    Resource.create({
        subjectName: req.body.subjectName,
        category: req.body.category,
        yearOfStudy: req.body.yearOfStudy,
    }).then((resource) => {
       res.status(200).send(res); 
    }, (err) =>{
      res.status(500).send(err);  
    });
});

app.post('/add-post', (req,res) => {
    Post.create({
        uploadedDate: req.body.uploadedDate,
        postName: req.body.postName,
        content: req.body.content,
        uploadingSource: req.body.uploadingSource,
        languageRo: req.body.languageRo
    }).then((post) => {
       res.status(200).send(res); 
    }, (err) =>{
      res.status(500).send(err);  
    });
});

app.post('/add-question', (req,res) => {
    Question.create({
        subject: req.body.subject,
        content: req.body.content,
    
    }).then((question) => {
       res.status(200).send(res); 
    }, (err) =>{
      res.status(500).send(err);  
    });
});

app.post('/add-answer', (req,res) => {
    Answer.create({
        subject: req.body.subject,
        content: req.body.content,
    }).then((answer) => {
       res.status(200).send(res); 
    }, (err) =>{
      res.status(500).send(err);  
    });
});


app.get('/get-all-resources', (req,res) =>{
    Resource.findAll().then((resources) =>{
        res.status(200).send(resources);    
    });
});

app.get('/get-all-posts', (req,res) =>{
    Post.findAll().then((posts) =>{
        res.status(200).send(posts);    
    });
});

app.get('/get-all-questions', (req,res) =>{
    Question.findAll().then((questions) =>{
        res.status(200).send(questions);    
    });
});

app.get('/get-all-answers', (req,res) =>{
    Answer.findAll().then((answers) =>{
        res.status(200).send(answers);    
    });
});

app.get('/get-all-users', (req,res) =>{
    User.findAll().then((user) =>{
        res.status(200).send(user);    
    });
});

app.put('/resources/:subjectName', (request, response) => {
    Resource.findById(request.params.subjectName).then((resource) => {
        if(resource) {
            resource.update(request.body).then((result) => {
                response.status(201).json(result)
            }).catch((err) => {
                console.log(err)
                response.status(500).send('database resource error')
            })
        } else {
            response.status(404).send('resource not found')
        }
    }).catch((err) => {
        console.log(err)
        response.status(500).send('database resource error')
    })
});

app.put('/posts/:postName', (request, response) => {
    Post.findById(request.params.postMessage).then((postName) => {
        if(postName) {
            postName.update(request.body).then((result) => {
                response.status(201).json(result)
            }).catch((err) => {
                console.log(err)
                response.status(500).send('database posts error')
            })
        } else {
            response.status(404).send('resource not found')
        }
    }).catch((err) => {
        console.log(err)
        response.status(500).send('database posts error')
    });
});

app.delete('/posts/:postName', (request, response) => {
    Post.findById(request.params.postName).then((postName) => {
        if(postName) {
            postName.destroy().then((result) => {
                response.status(204).send()
            }).catch((err) => {
                console.log(err)
                response.status(500).send('database posts delete error')
            })
        } else {
            response.status(404).send('resource not found')
        }
    }).catch((err) => {
        console.log(err)
        response.status(500).send('database posts delete error')
    })
})

app.delete('/answers/:subject', (request, response) => {
    Answer.findById(request.params.subject).then((subject) => {
        if(subject) {
            subject.destroy().then((result) => {
                response.status(204).send()
            }).catch((err) => {
                console.log(err)
                response.status(500).send('database answer delete error')
            })
        } else {
            response.status(404).send('resource not found')
        }
    }).catch((err) => {
        console.log(err)
        response.status(500).send('database answer delete error')
    })
})

//register
/*users.post("/register", (req, resp)=>{
    const today = new Date();
    const userData = {
        name: req.body.name,
        surname: req.body.surname,
        email: req.body.email,
        password: req.body.password,
        created: today
    } 
    User.findOne({where: {          //verifica daca mai exista un user cu acest email
        email:req.body.email
    }})
    .then(user=>{
        if(!user) {
            const hash = bcrypt.hashSync(userData.password, 10)
            userData.password = hash
            User.create(userData)
                .then(user=>{
                    let token = jwt.sign(user.dataValues, process.env.SECRET_KEY, {
                    expiresIn: 1440
                })
                resp.json({token: token})
            })
                .catch(err => {
                    resp.send('error: ' + err);
                })
            }else{
                resp.json({error: "User already exists"})
            }
        })
        .catch(err => {
            resp.send('error: ' + err);
        })
    
})*/

//login
users.post('/login', (req, resp)=>{
    User.findOne({
        where: {
            email: req.body.email
        }
    })
    .then(user=>{
        if(bcrypt.compareSync(req.body.password, user.password)) {
            let token = jwt.sign(user.dataValues, process.env.SECRET_KEY, {
                expiresIn: 1440
            })
            resp.json({token: token})
        }else {
            resp.send('User does not exist!')
        }
    })
    .catch(err=>{
        resp.send('error: ' + err)
    })
})



module.exports = users;

 app.listen(8080, ()=>{
console.log('Server started on port 3000...');
});
// apiRoutes(app);
// db.sequelize.sync().then(function(){
//     app.listen(3000, ()=>{
//     console.log('Server started on port 3000...');
// });
// });

// models -> User.js
// module.exports = function(sequelize,DataTypes)
// {
//     var User = sequelize.define('users', {
//          name: DataTypes.String,
//          surname: DataTypes.String,
//          email:DataTypes.String,
//          password:DataTypes.String
// });

// return User;
// }


// app.listen(8081, ()=>{
//     console.log('Server started on port 8081...');
// })